// USING DOM:
// Retrieve an element from webpage

const txt_first_name = document.querySelector("#txt-first-name")
const txt_last_name = document.querySelector("#txt-last-name")
const span_full_name = document.querySelector("#span-full-name")

/*
ALTERNATIVE IN RETRIEVING AN ELEMENT: getElement
	for id
		document.getElementById('txt-first-name');
	for class
		document.getElementsByClassName('tclass-name');
	for tags or element type:
		document.getElementsByTagName('input');

*/

// EVENT LISTENER: https://www.w3schools.com/js/js_htmldom_eventlisten
//  	-an interaction between the user and webpage.
// 		keyup - is a reserved key word.



/*txt_first_name.addEventListener("keyup", () => {
	span_full_name.innerHTML = txt_first_name.value
})



txt_first_name.addEventListener("keyup", (event) => {
	console.log(event.target)
	console.log(event.target.value)

	// event.target is just the same as the current element. which means this code below will work the same way
	// console.log(txt_first_name)
	// console.log(txt_first_name.value)
})*/


// ACTIVITY -------------------------------

const update_fullname = () => {
	let first_name = txt_first_name.value;
	let last_name = txt_last_name.value;

	span_full_name.innerHTML = `${first_name} ${last_name}`;
}

txt_first_name.addEventListener('keyup', update_fullname);
txt_last_name.addEventListener('keyup', update_fullname);


